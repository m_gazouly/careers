import ExploreSectionWrapper from "./Explore.style";

const ExploreSection = () => (
  <ExploreSectionWrapper>
    <div className="grid grid-rows-1 gap-4 md:grid-flow-col md:grid-cols-2">
      <div className="section-content">
        <h2 className="title">Explore Brimore</h2>
        <p className="description">
          We work in a cross-functional team of product owners, engineers, designers & analysts.
          Through our various software engineering fields; web, mobile, ERP, software quality, and
          data engineering we utilize our technical knowledge to devise optimal solutions for the daily real-life business challenges we face.
          Join us in being a part of how technology transforms lives, and to help us deliver our mission in transforming the retail industry in Egypt!
        </p>
      </div>
      <div className="section-video">
        <iframe src="https://www.youtube.com/embed/tgbNymZ7vqY">
        </iframe>
      </div>
    </div>
  </ExploreSectionWrapper>
)

export default ExploreSection