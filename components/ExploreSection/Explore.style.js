import styled from "styled-components";

const ExploreSectionWrapper = styled.div`
  min-height: 500px;
  padding: 20px 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  .section-content {
    .title {
      color: #2b3940!important;
      font-weight: 700;
      font-size: 2.25rem;
      line-height: 1.2;
      letter-spacing: -.9px;
      margin-bottom: 1.875rem!important;
    }
    .description {
      color: #6b6e6f;
      font-size: 1.125rem;
      line-height: 1.66;
      letter-spacing: -.09px;
    }
  }
  .section-video {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    overflow: hidden;
    width: 100%;
    padding-top: 56.25%;
    iframe {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      width: 100%;
      height: 100%;
    }
  }
  @media only screen and (max-width: 600px) {
    padding: 20px 15px;
  }
`

export default ExploreSectionWrapper