import styled from "styled-components";

const CategoriesWrapper = styled.div`
  padding: 25px 0;
  .title {
    color: #2b3940!important;
    font-weight: 700;
    font-size: 2.25rem;
    line-height: 1.2;
    letter-spacing: -.9px;
    margin-bottom: 1.875rem!important;
  }
  .description {
    color: #6b6e6f;
    font-size: 1.125rem;
    line-height: 1.66;
    letter-spacing: -.09px;
    margin-bottom: 1.65rem;
  }
`

const CategoryCardWrapper = styled.div`
  background: #fff;
  border-radius: 3px;
  border: 1px solid #e5e5e5;
  cursor: pointer;
  box-shadow: none;
  &:hover {
    box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;
    transition: box-shadow 0.3s ease-in-out;
  }
  .container {
    padding: 25px 0;
    .icon {
      background: red;
      width: 128px;
      height: 128px;
      padding: 10px;
      img {
        min-width: 64px !important;
        max-width: 64px !important;
        min-height: 64px !important;
        max-height: 64px !important;
      }
    }
  }
  @media only screen and (max-width: 600px) {
    margin: 0 20px;
  }
`
export { CategoryCardWrapper }

export default CategoriesWrapper