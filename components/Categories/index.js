import CategoriesWrapper, { CategoryCardWrapper } from "./Categories.style";
import Image from 'next/image'

const Categories = ({ children }) => (
  <CategoriesWrapper>
    { children }
  </CategoriesWrapper>
)

const CategoryCard = ({ title, icon, numberOfVacancies }) => (
  <CategoryCardWrapper>
    <div className="container">
      <div className="icon">
        <Image layout="fill" width={512} height={512} src={icon} />
      </div>
      <p>{ title }</p>
      {/* <p>{ icon }</p> */}
      <p>{ numberOfVacancies }</p>
    </div>
  </CategoryCardWrapper>
)

export { CategoryCard }
export default Categories