import Head from 'next/head'

const HeadTag = ({ title, keywords, description }) => {
  return (
    <Head>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <meta property="og:title" content={`${title} | Brimore`} />
      <meta property="og:site_name" content='Brimore' />
      <meta property="og:url" content='https://careers.brimore.com/' />
      <meta property="og:image" content='/favico.png' />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta charSet='utf-8' />
      <link rel='icon' href='/favico.png' />
      <title>{`${title} | Brimore`}</title>
    </Head>
  )
}

HeadTag.defaultProps = {
  title: 'Brimore',
  keywords: 'Mobile App, FMCG, Grocery, Delivery, Egypt',
  description: 'Mobile App, FMCG, Grocery, Delivery, Egypt',
}

export default HeadTag