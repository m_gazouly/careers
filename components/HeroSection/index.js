import HeroSection from "./HeroSection.style";
import HeroImage from '../../images/hero_bg.png'
import Image from 'next/image'

const Hero = ({ children }) => (
  <HeroSection>
    <div className="hero-img">
      <Image src={ HeroImage } />
    </div>
    <div className="container">
      { children }
    </div>
  </HeroSection>

)

export default Hero