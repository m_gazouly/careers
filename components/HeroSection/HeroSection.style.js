import styled from 'styled-components'

const HeroSection = styled.div`
  background: #EDF8F5;
  height: calc(0.75 * 100vh);
  position: relative;
  .hero-img {
    position: absolute;
    top: 0;
    right: 0;
    height: 100% !important;
    div {
      position: revert !important;
      img {
        height: 100% !important;
      }
    }
  }
  .container {
    width: 1200px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
    position: relative;
    .sm-title {
      color: #00b074;
      font-weight: 600;
      font-size: 1.125rem;
      line-height: 1.66;
      letter-spacing: -.09px;
      margin-bottom: 1.25rem!important;
    }
    .hero-title {
      color: #2b3940!important;
      font-weight: 700;
      font-size: 3.75rem;
      line-height: 1.2;
      letter-spacing: -.9px;
      margin-bottom: 1.875rem!important;

    }
    .hero-text {
      color: #6b6e6f;
      font-size: 1.125rem;
      line-height: 1.66;
      letter-spacing: -.09px;
    }
  }
  @media only screen and (max-width: 600px) {
    height: 100vh;
    .container {
      width: 100vw;
      padding: 0 15px;
      .hero-title {
        font-size: calc(1.5rem + 3vw);
      }
    }
  }
  @media only screen and (min-width: 767px) and (max-width: 1100px){
    .container {
      padding: 0 20px;
    }
  }
`

export default HeroSection
