import ContainerWrapper from "./Container.style";

const Container = ({ children }) => (
  <ContainerWrapper>
    { children }
  </ContainerWrapper>
)

export default Container