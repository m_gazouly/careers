import styled from "styled-components";

const ContainerWrapper = styled.div`
  width: 1200px;
  margin: 0 auto;
  @media only screen and (max-width: 600px) {
    width: 100vw;
  }
`

export default ContainerWrapper