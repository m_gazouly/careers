import NavbarWrapper from "./Navbar.style";
import Image from 'next/image'
import Link from 'next/link'

const Navbar = () => (
  <NavbarWrapper>
    <div className="container">
      <div className="logo">
        <Image layout="responsive" objectFit="contain" src="/favico.png" width={1} height={1} />
      </div>
      <Link href="https://brimore.com/#top_four_feature">
        <a className="about-us" target="_blank">
          About Us
        </a>
      </Link>
    </div>
  </NavbarWrapper>
)

export default Navbar