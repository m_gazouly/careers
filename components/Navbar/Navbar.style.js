import styled from 'styled-components'

const NavbarWrapper = styled.header`
  background: #fff;
  box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
  height: 87px;
  position: sticky;
  width: 100%;
  top: 0;
  z-index: 1000;
  .container {
    width: 1200px;
    margin: 0 auto;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    height: 100%;
    .logo {
      width: 60px;
      height: inherit;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
    .about-us {
      color: #2b3940;
      font-size: 14px;
      font-weight: 700;
      text-transform: uppercase;
    }
  }
  @media only screen and (max-width: 600px) {
    height: 65px;
    .container {
      width: 100vw;
      justify-content: center;
      .logo {
        width: 50px;
        height: inherit
      }
      .about-us {
        display: none;
      }
    }
  }
`

export default NavbarWrapper