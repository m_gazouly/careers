// import Head from 'next/head'
import Head from '../components/SEO/Head'
import HeroSection from '../components/HeroSection'
import Navbar from '../components/Navbar'
import Container from '../components/Container'
import ExploreSection from '../components/ExploreSection'
import Categories, { CategoryCard } from '../components/Categories'

// Images
import FrontendIcon from '../images/android.png'

const Home = () => {
  const cards = [
    { title: 'Frontend', icon: FrontendIcon, vacancies: 5 },
    { title: 'Backend', icon: FrontendIcon, vacancies: 4 },
    { title: 'Mobile', icon: FrontendIcon, vacancies: 3 },
    { title: 'ERP', icon: FrontendIcon, vacancies: 2 },
    { title: 'Data Science', icon: FrontendIcon, vacancies: 1 },
    { title: 'Data Engineering', icon: FrontendIcon, vacancies: 0 }
  ]
  return (
    <>
      <Head title='Careers' />
      <Navbar />
      <HeroSection>
        <h3 className="sm-title">#4923 jobs are available right now</h3>
        <div className="grid md:grid-cols-2">
          <h1 className="hero-title">Find the most exciting jobs.</h1>
        </div>
        <div className="grid md:grid-cols-2">
          <p className="hero-text">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative to</p>
        </div>
      </HeroSection>
      <Container>
        <ExploreSection />
        <hr />
        <div className="text-center">
          <Categories>
            <h2 className="title">Explore Brimore vacancies</h2>
            <p className="description">lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume
              lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume
              lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume lorem ipsume 
            </p>
            <div className="grid grid-rows-2 gap-4 md:grid-flow-col md:grid-cols-3">
              {
                cards.map(({ title, icon, vacancies }, i) => (
                  <CategoryCard
                    key={i}
                    title={title}
                    icon={icon}
                    numberOfVacancies={vacancies}
                  />
                ))
              }
            </div>
          </Categories>
        </div>
      </Container>
    </>
  )
}
export default Home
